package application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class HexaFileReader {
    private final BufferedReader br;

    public HexaFileReader(String fileName) throws FileNotFoundException {
        br = new BufferedReader(new FileReader(fileName));
    }

    String getNextEntry() throws IOException {
        String line;
        if ((line = br.readLine()) == null) return null;
        return line;
    }

    public long translateHexa(String line) {
        String hexa = line.substring(0,8);
        return Long.decode('#' + hexa);
    }

    public boolean translateReadWrite(String line) {
        if (line.charAt(9) == 'R') {
            return true;
        }
        else if (line.charAt(9) == 'W') {
            return false;
        }
        else {
            throw new RuntimeException("Last character is not R or W");
        }
    }
}
