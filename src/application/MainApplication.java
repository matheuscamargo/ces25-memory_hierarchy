package application;

import memorymanagement.MMU;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainApplication {
    private static MMU mmu;
    private static int minClocks = 10000000;

    private static void runTest(int l1Size, int l1Associativity,
                    int l2Size, int l2Associativity, int blockSize) throws IOException {
        HexaFileReader reader = new HexaFileReader("res/gcc.trace");
        mmu = new MMU(l1Size, l1Associativity,
                l2Size, l2Associativity, blockSize);
        String line;
        while ((line = reader.getNextEntry()) != null) {
            long address = reader.translateHexa(line);
            boolean read = reader.translateReadWrite(line);
            mmu.access(address, read);
        }
        mmu.printStatistics();
    }

    public static void main(String[] args) throws IOException {
        List<Integer> l1Sizes = new ArrayList<>();
        //l1Sizes.add(16 * 1024);
        l1Sizes.add(32 * 1024);
        //l1Sizes.add(64 * 1024);
        List<Integer> l2Sizes = new ArrayList<>();
        l2Sizes.add(512 * 1024);
        l2Sizes.add(1024 * 1024);
        l2Sizes.add(2048 * 1024);
        l2Sizes.add(4096 * 1024);
        l2Sizes.add(8192 * 1024);
        l2Sizes.add(2 * 8192 * 1024);
        List<Integer> l1Associativities = new ArrayList<>();
        l1Associativities.add(4);
        l1Associativities.add(8);
        l1Associativities.add(16);
        l1Associativities.add(32);
        l1Associativities.add(64);
        List<Integer> l2Associativities = new ArrayList<>();
        l2Associativities.add(4);
        l2Associativities.add(8);
        l2Associativities.add(16);
        l2Associativities.add(32);
        l2Associativities.add(64);
        l2Associativities.add(128);
        l2Associativities.add(256);
        l2Associativities.add(512);
        l2Associativities.add(1024);
        l2Associativities.add(8 * 1024);
        List<Integer> blockSizes = new ArrayList<>();
        blockSizes.add(16);
        blockSizes.add(32);
        blockSizes.add(64);
        blockSizes.add(128);
        blockSizes.add(256);
        blockSizes.add(512);

        for (int l1Associativity : l1Associativities) {
            for (int l2Associativity: l2Associativities) {
                //System.out.println(l1Size + " " + l2Size + " " + l1Associativity + " " + l2Associativity + " " + blockSize);
                //runTest(32*1024, l1Associativity, 4096*1024, l2Associativity, 512);

            }
            System.out.println();
        }
        runTest(32*1024, 8, 4096*1024, 16, 64);
        runTest(32*1024, 32, 4096*1024, 32, 512);
    }
}
