package memorymanagement;

public class L2 extends Cache{

    public L2(int size, int blockSize, int associativity) {
        super(size, blockSize, associativity, Math.max(2 * size / (4096 * 1024), 4), Math.max(4 * size / (4096 * 1024), 2));
    }

    void markEntryDirty(long address) {
        int set = (int) getSet(address);
        long tag = getTag(address);

        Entry toLook = new Entry(tag, false);

        for (Entry entry : queues.get(set)) {
            if (entry.equals(toLook)) {
                entry.dirty = true;
                break;
            }
        }
    }
    
}
