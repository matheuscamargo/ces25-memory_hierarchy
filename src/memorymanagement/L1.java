package memorymanagement;

public class L1 extends Cache {

    public L1(int size, int blockSize, int associativity) {
        super(size, blockSize, associativity, 1, 2);
    }

}
