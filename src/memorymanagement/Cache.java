package memorymanagement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

abstract class Cache {
    private final int blockSize;
    private final int associativity;

    final int comparisonTime;
    final int accessTime;
    private final int numberOfSets;
    final List<ArrayBlockingQueue<Entry>> queues;

    Cache (int size, int blockSize, int associativity, int comparisonTime, int accessTime) {
        this.blockSize = blockSize;
        this.associativity = associativity;

        this.comparisonTime = comparisonTime;
        this.accessTime = accessTime;

        int numberOfBlocks = size / blockSize;
        numberOfSets = numberOfBlocks / associativity;
        queues = new ArrayList<>(numberOfSets);
        for (int i = 0; i < numberOfSets; i++) {
            queues.add(new ArrayBlockingQueue<>(associativity));
        }
    }

    public long getSet(long address) {
        int offsetSize = (int) (Math.log(blockSize) / Math.log(2));
        long setOnTheRight = address >> offsetSize;
        long mask = numberOfSets - 1;
        return mask & setOnTheRight;
    }

    public long getTag(long address) {
        int offsetSize = (int) (Math.log(blockSize) / Math.log(2));
        int setSize = (int) (Math.log(numberOfSets) / Math.log(2));
        return address >> (offsetSize + setSize);
    }

    boolean contains(long address) {
        int set = (int) getSet(address);
        long tag = getTag(address);
        Entry toLook = new Entry(tag, false);
        return queues.get(set).contains(toLook);
    }

    boolean insertAndCheckDirty(long address) {
        int set = (int) getSet(address);
        long tag = getTag(address);

        boolean wasDirty = false;
        if (queues.get(set).size() == associativity) {
            Entry entry = queues.get(set).poll();
            wasDirty = entry.dirty;
        }
        queues.get(set).offer(new Entry(tag, false));
        return wasDirty;
    }
}
