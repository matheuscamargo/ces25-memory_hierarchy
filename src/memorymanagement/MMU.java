package memorymanagement;

public class MMU {
    private final L1 l1;// = new L1(32 * 1024, 64, 8);
    private final L2 l2;// = new L2(4096 * 1024, 64, 16);

    private long l1ReadHits = 0;
    private long l2ReadHits = 0;
    private long l2ReadMisses = 0;
    private long l1WriteHitsWithl2Hit = 0;
    private long l1WriteHitsWithl2Miss = 0;
    private long l2WriteHits = 0;
    private long l2WriteMisses = 0;
    private long l2WritingBack = 0;

    private long l1ReadHitTime = 0;
    private long l2ReadHitTime = 0;
    private long l2ReadMissTime = 0;
    private long l1WriteHitsWithl2HitTime = 0;
    private long l1WriteHitsWithl2MissTime = 0;
    private long l2WriteHitTime = 0;
    private long l2WriteMissTime = 0;
    private long l2WritingBackTime = 0;


    public MMU(int l1Size, int l1Associativity,
               int l2Size, int l2Associativity, int blockSize) {
        l1 = new L1(l1Size, blockSize, l1Associativity);
        l2 = new L2(l2Size, blockSize, l2Associativity);

        l1ReadHitTime = l1.comparisonTime + l1.accessTime;
        l2ReadHitTime = l2.comparisonTime + l2.accessTime;
        l2ReadMissTime = 60;
        l1WriteHitsWithl2HitTime = l2.comparisonTime + l2.accessTime;
        l1WriteHitsWithl2MissTime = 60 + l2.comparisonTime;
        l2WriteHitTime = l2.comparisonTime + l2.accessTime;
        l2WriteMissTime = 60 + l2.comparisonTime;
        l2WritingBackTime = 60;
    }

    public void printStatistics() {
//        System.out.println("l1ReadHits = " + l1ReadHits +
//                " * 3 clocks = " + 3 * l1ReadHits);
//        System.out.println("l2ReadHits = " + l2ReadHits +
//                " * 6 clocks = " + 6 * l2ReadHits);
//        System.out.println("l2ReadMisses = " + l2ReadMisses +
//                " * 61 clocks = " + 61 * l2ReadMisses);
//        System.out.println("l1WriteHitsWithl2Hit = " + l1WriteHitsWithl2Hit +
//                " * 6 clocks = " + 6 * l1WriteHitsWithl2Hit);
//        System.out.println("l1WriteHitsWithl2Miss = " + l1WriteHitsWithl2Miss +
//                " * 61 clocks = " + 61 * l1WriteHitsWithl2Miss);
//        System.out.println("l2WriteHits = " + l2WriteHits +
//                " * 6 clocks = " + 6 * l2WriteHits);
//        System.out.println("l2WriteMisses = " + l2WriteMisses +
//                " * 61 clocks = " + 61 * l2WriteMisses);
//        System.out.println("l2WritingBack = " + l2WritingBack +
//                " * 60 clocks = " + 60 * l2WritingBack);
        System.out.print((l1ReadHitTime * l1ReadHits +
                          l2ReadHitTime * l2ReadHits +
                          l2ReadMissTime * l2ReadMisses +
                          l1WriteHitsWithl2HitTime * l1WriteHitsWithl2Hit +
                          l1WriteHitsWithl2MissTime * l1WriteHitsWithl2Miss +
                          l2WriteHitTime * l2WriteHits +
                          l2WriteMissTime * l2WriteMisses +
                          l2WritingBackTime * l2WritingBack) + " ");
    }

    public void access(long address, boolean read) {
        if (read) {
            if (l1.contains(address)) {
                L1ReadHit();
            }
            else if (l2.contains(address)) {
                L2ReadHit(address);
            }
            else {
                L2ReadMiss(address);
            }
        }
        else {
            if (l1.contains(address)) {
                if (l2.contains(address)) {
                    L1WriteHitWithL2Hit(address);
                }
                else {
                    L1WriteHitWithL2Miss();
                }
            }
            else if (l2.contains(address)) {
                L2WriteHit(address);
            }
            else {
                L2WriteMiss();
            }
        }
    }

    private void L1ReadHit() {
        l1ReadHits++;
    }

    private void L2ReadHit(long address) {
        l2ReadHits++;
        l1.insertAndCheckDirty(address);
    }

    private void L2ReadMiss(long address) {
        l2ReadMisses++;
        l1.insertAndCheckDirty(address);
        if (l2.insertAndCheckDirty(address)) {
            l2WritingBack++;
        }
    }

    private void L1WriteHitWithL2Hit(long address) {
        l1WriteHitsWithl2Hit++;
        l2.markEntryDirty(address);
    }

    private void L1WriteHitWithL2Miss() {
        l1WriteHitsWithl2Miss++;
    }

    private void L2WriteHit(long address) {
        l2WriteHits++;
        l1.insertAndCheckDirty(address);
        l2.markEntryDirty(address);
    }

    private void L2WriteMiss() {
        l2WriteMisses++;
    }
}
