package memorymanagement;

class Entry {
    public final long tag;
    public boolean dirty;

    public Entry(long tag, boolean dirty) {
        this.tag = tag;
        this.dirty = dirty;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Entry.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Entry other = (Entry) obj;
        if (this.tag != other.tag) {
            return false;
        }
        return true;
    }
}
